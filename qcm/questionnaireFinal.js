const xhttp = new XMLHttpRequest()
let timeStart = null
let timeEnd = null
let div = document.createElement('div')
div.style.padding = '20px'
div.style.display = "block"
div.style.width = '640px'
div.style.marginLeft = 'auto'
div.style.marginRight = 'auto'
div.id = 'divQuestionnaire'
document.getElementsByTagName('body')[0].appendChild(div)
let divQuestionnaire = document.getElementById('divQuestionnaire')
divQuestionnaire.style.display = "none"
let nombreQuestions = 5
let NombrePropositions = 4
let lesReponses = null
let LesQuestionsPosees = null
let LesBonnesReponsesAuxQuestionsPosees = null
let element = []
let candidat = {
    name: null,
    score: 0,
    email: null
}

let valider = document.getElementById("valider")

let fin = document.getElementById("fin")


valider.addEventListener("click", function () {
    timeEnd = Date.now() - timeStart
    nombrePoints(LesQuestionsPosees, NombrePropositions, LesBonnesReponsesAuxQuestionsPosees, candidat)
    divQuestionnaire.style.display = "none"
    valider.style.display = "none"
    fin.style.display = "block"
    Email.send({
        SecureToken: "c00033cf-c77b-430e-8a0a-1fc28f063a84",
        To: 'thierry.sautiere@ac-lille.fr',
        From: "darkquizduez@gmail.com",
        Subject: `${candidat.name}`,
        Body: `${candidat.name} a obtenu ${candidat.score}/${nombreQuestions} en ${timeEnd/1000} secondes`
    }).then(
        message => console.log(message)
    );
    Email.send({
        SecureToken: "c00033cf-c77b-430e-8a0a-1fc28f063a84",
        To: `${candidat.email}`,
        From: "darkquizduez@gmail.com",
        Subject: `${candidat.name}`,
        Body: `${candidat.name}, tu as obtenu ${candidat.score}/${nombreQuestions} en ${timeEnd/1000} secondes`
    }).then(
        message => console.log(message)
    );

})
candidat.email = prompt("Give your email : ")
timeStart = Date.now();
text.style.display = "block"
candidat.name = prompt("Donner votre nom")
div.style.display = "block"
valider.style.display = "block"
console.log(candidat.name)

function lesBonnesReponses(LesQuestionsPosees) {
    let reponse = []
    for (let i = 0; i < LesQuestionsPosees.length; i++) {
        reponse.push(LesQuestionsPosees[i] / (NombrePropositions+1))
    }
    return reponse
}

function TirageQuestion(nombreQuestions, fichierQuestionnaire, lesReponses, NombrePropositions) {

    let LesQuestionsPosees = []
    let aleatoire = Math.floor(Math.random() * (fichierQuestionnaire.length - 2))
    let choixQuestion = aleatoire % 5 ? (aleatoire - aleatoire % (NombrePropositions+1)) : aleatoire
    LesQuestionsPosees.push(choixQuestion)
    for (let j = 0; j < nombreQuestions - 1; j++) {
        while (!LaQuestionEstElleDejaChoisie(choixQuestion, LesQuestionsPosees)) {
            aleatoire = Math.floor(Math.random() * (fichierQuestionnaire.length - 2))
            choixQuestion = aleatoire % (NombrePropositions+1) ? (aleatoire - aleatoire % (NombrePropositions+1)) : aleatoire
        }
        LesQuestionsPosees.push(choixQuestion)
    }
    return LesQuestionsPosees
}

function LaQuestionEstElleDejaChoisie(choixQuestion, LesQuestionsPosees) {
    for (let i = 0; i < LesQuestionsPosees.length; i++) {
        if (choixQuestion == LesQuestionsPosees[i]) {
            return false
        }
    }
    return true
}

function afficherLesQuestions(fichierQuestionnaire, LesQuestionsPosees, NombrePropositions, balise) {


    for (let i = 0; i < LesQuestionsPosees.length; i++) {
        let caseCochee = Math.floor(Math.random() * NombrePropositions) + 1
        for (let j = 0; j < NombrePropositions + 1; j++) {
            element.push(j == 0 ? document.createElement('p') : document.createElement('input'))
            if (j != 0) {
                element[i * (NombrePropositions+1) + j].type = "radio"
                element[i * (NombrePropositions+1) + j].name = `q${i}`
                element[i * (NombrePropositions+1) + j].id = `q${i}${j}`
                if (j == caseCochee) {
                    element[i * 5 + j].checked = 'checked'
                }
                let spam = document.createElement('spam')
                spam.id = `enonce${i}${j}`
                spam.style.marginLeft = '15px'
                spam.style.textAlign = 'center'
                divQuestionnaire.appendChild(element[i * (NombrePropositions+1) + j])
                divQuestionnaire.appendChild(spam)
                spam.innerHTML = fichierQuestionnaire[LesQuestionsPosees[i] + j] + '<br>'
            } else {
                element[i * (NombrePropositions+1) + j].style.display = 'block'
                element[i * (NombrePropositions+1) + j].style.color = 'black'
                element[i * (NombrePropositions+1) + j].style.padding = '10px'
                element[i * (NombrePropositions+1) + j].style.borderStyle = 'groove'
                element[i * (NombrePropositions+1) + j].style.width = '600px'
                element[i * (NombrePropositions+1) + j].style.marginLeft = 'auto'
                element[i * (NombrePropositions+1) + j].style.marginRight = 'auto'
                element[i * (NombrePropositions+1) + j].style.boxShadow = '8px 8px 8px rgba(120,20,50,0.5)'
                element[i * (NombrePropositions+1) + j].style.backgroundColor = 'rgba(190,220,210,0.8)'
                divQuestionnaire.appendChild(element[i * (NombrePropositions+1) + j])
                element[i * (NombrePropositions+1) + j].innerHTML = `QUESTION ${i+1} <br><br> ${fichierQuestionnaire[LesQuestionsPosees[i]]}`
            }
        }
    }
}

function nombrePoints(LesQuestionsPosees, NombrePropositions, LesBonnesReponsesAuxQuestionsPosees, candidat) {
    for (let i = 0; i < LesQuestionsPosees.length; i++) {
        for (let j = 1; j < NombrePropositions + 1; j++) {
            if (element[i * (NombrePropositions+1) + j].checked) {
                if (j == lesReponses[LesBonnesReponsesAuxQuestionsPosees[i]])
                    candidat.score++
            }
        }
    }
    return candidat.score
}

function controleDesReponses(lesReponses, LesQuestionsPosees, LesBonnesReponsesAuxQuestionsPosees) {
    let reponses = []
    for (let i = 0; i < LesQuestionsPosees.length; i++) {
        reponses.push(lesReponses[LesBonnesReponsesAuxQuestionsPosees[i]])
    }
    return reponses
}

xhttp.onreadystatechange = function () {

    if (this.readyState == 4 && this.status == 200) {
        MathJax.Hub.Config({
            "HTML-CSS": {
                preferredFont: "TeX",
                scale: 100
            }
        });

        let fichierQuestionnaire = this.responseText.split('\n')
        lesReponses = fichierQuestionnaire[fichierQuestionnaire.length - 1].split(';')
        LesQuestionsPosees = TirageQuestion(nombreQuestions, fichierQuestionnaire, lesReponses, NombrePropositions)
        LesBonnesReponsesAuxQuestionsPosees = lesBonnesReponses(LesQuestionsPosees)
        afficherLesQuestions(fichierQuestionnaire, LesQuestionsPosees, NombrePropositions, document.getElementById('text'))
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    }
}
xhttp.open("GET", "questionnaireshell.txt", true)
xhttp.send()
