Si un fichier a les permissions suivantes 764 donc...
Tout le monde peut lire, le groupe ne peut que exécuter et le propriétaire peut lire et écrire.
Chacun peut lire et écrire, mais le propriétaire seul peut exécuter.
Tout le monde peut lire, le groupe y compris le propriétaire peut écrire, le propriétaire seul peut exécuter.
Tout le monde peut lire, le groupe peut écrire et lire et le propriétaire peut lire, écrire et exécuter.
Quelle est la représentation octale de ces permissions -rwx r-s r–?
0777
2766
2744
2754
Les permissions -rwxr–-r–- représentées par la valeur octale est :
777
666
744
711
Laquelle de ces commandes va définir les permissions sur le fichier « myfile »: lire et écrire pour le propriétaire(user), lire pour le groupe(group) et rien pour autres(others)?
chmod 046 file
chmod 640 file
chmod 310 file
chmod rw rw file
Quelle commande crée un fichier vide si le fichier n’existe pas?
cat
touch
read
ed
Quelle commande est utilisée pour afficher la valeur octale d’un fichier texte?
oct
octal
text_oct
od
Quelle commande est utilisée pour modifier les autorisations des fichiers et des répertoires?
mv
chgrp
chmod
set
Le droit de lecture r permet pour un fichier de :
le consulter, copier ou modifier
le consulter, copier ou exécuter
le consulter ou le copier
le consulter uniquement
Le point "." désigne :
la racine
un caractère générique
le dossier courant
le dossier parent
Si les permissions -rwxr-xr-- sont accordées au groupe gp1 pour un fichier celui-ci a les droits :
de lecture et d'exécution
de lecture uniquement
d'exécution uniquement
de lecture, écriture et exécution
Comment se nomme la racine des dossiers ?
:
/
\
root
Si un fichier a les permissions -rw-r--r-- , son propriétaire a les droits :
de lecture uniquement
d'exécution uniquement
de lecture et d'écriture
de lecture et d'exécution
La commande mv rep1 rep2
renomme rep1 en rep2
copie rep1 vers rep2
déplace rep1 dans rep2
déplace rep1 et rep2 dans le dossier courant
4;4;3;2;2;4;3;3;3;1;2;3;1